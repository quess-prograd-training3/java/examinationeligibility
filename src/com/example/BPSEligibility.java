package com.example;

public class BPSEligibility {
    boolean computerKnowledge;
    public BPSEligibility(boolean computerKnowledge) {
        this.computerKnowledge=computerKnowledge;
    }
    public boolean eligibility(){
        if(computerKnowledge == false){
            return false;
        }
        return true;
    }
}
