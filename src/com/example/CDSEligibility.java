package com.example;

public class CDSEligibility {
    int age;
    String maritalStatus;
    Boolean tooth, vision;

    public CDSEligibility(int age, String maritalStatus, Boolean tooth, Boolean vision) {
        this.age = age;
        this.maritalStatus = maritalStatus;
        this.tooth = tooth;
        this.vision = vision;
    }

    public boolean eligibility(){
        if(!(age >= 20 && age <= 24)){
            return false;
        }
        if(!(maritalStatus.equals("married"))){
            return false;
        }
        if(!(tooth == true) && (vision == true)){
            return false;
        }
        return true;
    }
}
