package com.example;

public class IASEligibility {
    private int attempts;

    public IASEligibility(int attempts) {
        this.attempts = attempts;
    }
    public boolean eligibility(){
        if(attempts > 6){
            return false;
        }
        return true;
    }
}
