import com.example.BPSEligibility;
import com.example.CDSEligibility;
import com.example.IASEligibility;


public class ExaminationEligibility {
    public static void main(String[] args) {
        BPSEligibility bpsEligibility = new BPSEligibility(false);
        if (bpsEligibility.eligibility()) {
            System.out.println("yes eligible for BPS");
        } else {
            System.out.println("Not eligible for BPS");
        }

        CDSEligibility cdsEligibility = new CDSEligibility(21, "married", true, true);
        if (cdsEligibility.eligibility()) {
            System.out.println("yes eligible for CDS");
        } else {
            System.out.println("Not eligible for CDS");
        }

        IASEligibility iasEligibility = new IASEligibility(6);
        if (iasEligibility.eligibility()) {
            System.out.println("yes eligible for IAS");
        } else {
            System.out.println("Not eligible for IAS");
        }
    }


}
